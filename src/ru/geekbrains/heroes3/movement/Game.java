package ru.geekbrains.heroes3.movement;

import ru.geekbrains.heroes3.movement.Units.MadeMoveForBasicUnit;
import ru.geekbrains.heroes3.movement.Units.Unit;

import java.util.Scanner;

/**
 * Основной класс
 */
public class Game {
    private char[][] field;
    private Unit unit; // переменная юнита

    public Game() {
        field = new char[24][24];
    }

    /**
     * инициализируем поле и ставим юнит
     */
    public void initGame() {
        for (int i = 2; i < field.length - 2; i++) {
            if ((i & 1) == 0) {
                for (int j = 2; j < field.length - 2; j += 2) {
                    field[i][j] = '.';
                }
            } else {
                for (int j = 3; j < field.length - 2; j += 2) {
                    field[i][j] = '.';
                }
            }
        }
        /*ставим юнит по запросу пользователя*/
        showField();
        setUnitByUserRequest();
    }

    /**
     * "основной" метод класса
     */
    public void startGame() {
        for (int i = 0; i < 10; i++) {
            unit.drawAvailableArea(field);
            showField();
            moveUnitByUserRequest();
            clearFieldFromMovements();
        }
    }

    /**
     * показываем поле
     */
    public void showField() {
        System.out.println("Координаты от 2 до 20 по нечетной строке");
        System.out.println("Координаты от 3 до 21 по четной строке");
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                System.out.printf(field[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * установка юнита пользователем
     */
    public void setUnitByUserRequest() {
        boolean isOk = false;
        while (isOk == false) {
            int selectedRow = 0;
            int selectedColumn = 0;
            int rangeOfMove = 0;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите координаты отряда через пробел и дальность хода");
            try {
                selectedRow = scanner.nextInt();
            } catch (Exception e) {
                continue;
            }
            try {
                selectedColumn = scanner.nextInt();
            } catch (Exception e) {
                continue;
            }
            try {
                rangeOfMove = scanner.nextInt();
            } catch (Exception e) {
                continue;
            }
            /*проверка на корректность значения, чтобы не допускать ArrayOutOfBonds*/
            if (selectedRow > 1 && selectedRow < 22 && selectedColumn > 1 && selectedColumn < 22) {
                if (field[selectedRow][selectedColumn] == '.' && rangeOfMove > 0) {
                    unit = new Unit();
                    unit.setMovable(new MadeMoveForBasicUnit());
                    unit.setRangeOfUnit(rangeOfMove);
                    boolean isSet = unit.setUnit(selectedRow, selectedColumn, field);
                    if (isSet == true) {
                        isOk = true;
                    }
                }
            }
        }
    }

    /**
     * двигаем юнит по запросу пользователя
     */
    public void moveUnitByUserRequest() {
        boolean isOk = false;
        int selectedRow = 0;
        int selectedColumn = 0;
        int rangeOfMove = 0;
        while (isOk == false) {
            System.out.println("Введите новые координаты отряда");
            Scanner scanner = new Scanner(System.in);
            try {
                selectedRow = scanner.nextInt();
            } catch (Exception e) {
                continue;
            }
            try {
                selectedColumn = scanner.nextInt();
            } catch (Exception e) {
                continue;
            }
            if (selectedRow > 1 && selectedRow < 22 && selectedColumn > 1 && selectedColumn < 22) {
                boolean isMove = unit.makeMove(selectedRow, selectedColumn, field);
                if (isMove == true) {
                    isOk = true;
                }
            }
        }
    }

    /**
     * очистка поля от области доступных ходов
     */
    public void clearFieldFromMovements() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                if (field[i][j] == '#') {
                    field[i][j] = '.';
                }
            }
        }
    }
}

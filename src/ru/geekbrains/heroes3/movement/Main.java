package ru.geekbrains.heroes3.movement;

/**
 * @author Eugeny Karpov`
 */
public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        game.initGame();
        game.startGame();
    }
}
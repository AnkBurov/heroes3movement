package ru.geekbrains.heroes3.movement.Units;

import java.util.ArrayList;
import java.util.List;

public class MadeMoveForBasicUnit implements Movable {
    @Override
    public boolean makeMove(int selectedRow, int selectedColumn, int currentRow, int currentColumn, char[][] field) {
        if (field[selectedRow][selectedColumn] == '#') {
            field[selectedRow][selectedColumn] = 'X';
            field[currentRow][currentColumn] = '.';
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setUnit(int selectedRow, int selectedColumn, char[][] field) {
        if (field[selectedRow][selectedColumn] == '.') {
            field[selectedRow][selectedColumn] = 'X';
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void drawAvailableArea(int rangeOfUnit, int currentRow, int currentColumn, char[][] field) {
        if (rangeOfUnit == 0) {
            return;
        } else {
            /*количество возможных ходов = размеру коллекции*/
            List<int[]> availableMoves = generateMoves(currentRow, currentColumn, field);
            while (!availableMoves.isEmpty()) {
                int[] coordinates = availableMoves.get(0);
                availableMoves.remove(0);
                /*ставим решетку как в доступное поле*/
                field[coordinates[0]][coordinates[1]] = '#';
                drawAvailableArea(rangeOfUnit - 1, coordinates[0], coordinates[1], field);
            }
        }
    }

    @Override
    public List<int[]> generateMoves(int row, int column, char[][] field) {
        List<int[]> availableMoves = new ArrayList<>();
        /*слева внизу*/
        if (field[row + 1][column - 1] == '.' || field[row + 1][column - 1] == '#') {
            availableMoves.add(new int[]{row + 1, column - 1});
        }
        /*слева*/
        if (field[row][column - 2] == '.' || field[row][column - 2] == '#') {
            availableMoves.add(new int[]{row, column - 2});
        }
        /*слева вверху*/
        if (field[row - 1][column - 1] == '.' || field[row - 1][column - 1] == '#') {
            availableMoves.add(new int[]{row - 1, column - 1});
        }
        /*справа внизу*/
        if (field[row + 1][column + 1] == '.' || field[row + 1][column + 1] == '#') {
            availableMoves.add(new int[]{row + 1, column + 1});
        }
        /*справа*/
        if (field[row][column + 2] == '.' || field[row][column + 2] == '#') {
            availableMoves.add(new int[]{row, column + 2});
        }
        /*справа вверху*/
        if (field[row - 1][column + 1] == '.' || field[row - 1][column + 1] == '#') {
            availableMoves.add(new int[]{row - 1, column + 1});
        }
        return availableMoves;
    }
}

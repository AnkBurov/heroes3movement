package ru.geekbrains.heroes3.movement.Units;

import java.util.List;

public interface Movable {
    /**
     * метод движения юнита из текущих координат в новые координаты по заданному полю
     *
     * @return возвращает истину или ложь в зависимости от успеха или неудачи движения
     */
    boolean makeMove(int selectedRow, int selectedColumn, int currentRow, int currentColumn, char[][] field);

    /**
     * метод установки юнита в заданные координаты
     *
     * @return возвращает истину или ложь в зависимости от успеха установки юнита
     */
    boolean setUnit(int selectedRow, int selectedColumn, char[][] field);

    /**
     * рекурсивный метод рисования области доступных ходов для юнита. Для каждой входной координаты вызывается метод
     * generateMoves и возвращается коллекция доступных ходов. Для каждого элемента коллекции рекурсивно вызывается
     * метод drawAvailableArea с дальностью хода -1.
     */
    void drawAvailableArea(int rangeOfUnit, int currentRow, int currentColumn, char[][] field);

    /**
     * метод генерации доступных ходов из каждой текущей ячейки
     *
     * @return коллекция координат доступных ходов для текущей соты-ячейки
     */
    List<int[]> generateMoves(int row, int column, char[][] field);
}

package ru.geekbrains.heroes3.movement.Units;

public class Unit {
    protected int currentRow;
    protected int currentColumn;
    protected int rangeOfUnit;
    protected Movable movable; // композиция стратегии Movable

    public Unit() {
    }

    public void setCurrentRow(int currentRow) {
        this.currentRow = currentRow;
    }

    public void setCurrentColumn(int currentColumn) {
        this.currentColumn = currentColumn;
    }

    public void setMovable(Movable movable) {
        this.movable = movable;
    }

    public void setRangeOfUnit(int rangeOfUnit) {
        this.rangeOfUnit = rangeOfUnit;
    }

    public boolean makeMove(int selectedRow, int selectedColumn, char[][] field) {
        boolean isSet = movable.makeMove(selectedRow, selectedColumn, currentRow, currentColumn, field);
        if (isSet == true) {
            this.currentRow = selectedRow;
            this.currentColumn = selectedColumn;
            return true;
        } else {
            System.out.println("Ход невозможен в эту соту");
            return false;
        }
    }

    public boolean setUnit(int selectedRow, int selectedColumn, char[][] field) {
        boolean isSet = movable.setUnit(selectedRow, selectedColumn, field);
        if (isSet == true) {
            this.currentRow = selectedRow;
            this.currentColumn = selectedColumn;
            return true;
        } else {
            System.out.println("Поставить юнит в эту соту невозможно");
            return false;
        }
    }

    public void drawAvailableArea(char[][] field) {
        movable.drawAvailableArea(rangeOfUnit, currentRow, currentColumn, field);
    }
}
